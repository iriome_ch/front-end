<?php
include __DIR__."/utils/utils.php";
$errors = array();

if(empty($_POST["f-name"])||!isset($_POST["f-name"])){
array_push($errors,"First name is required");
}else{
    $fn = $_POST["f-name"];
}
if(empty($_POST["email"])||!isset($_POST["email"])){
array_push($errors,"Email is required");
}elseif(!filter_var($_POST["email"],FILTER_VALIDATE_EMAIL)){
array_push($errors,"The email Format is not right");
}else{
    $email= $_POST["email"];
}
if(empty($_POST["subject"])||!isset($_POST["subject"])){
array_push($errors,"Subject is required");
}else{
    $sub= $_POST['subject'];
}




function f_error($errors){
    $h = count($errors);
    for ($i=0; $i < $h; $i++) { 
        if (empty($errors)) {return "  ";}elseif($errors[$i]==="First name is required"){
            return $errors[$i];
        }else { return " ";}
    }
    
}

function s_error($errors){
    $h = count($errors);
    for ($i=0; $i < $h; $i++) { 
        if (empty($errors)) {return "  ";}elseif($errors[$i]==="Subject is required"){
            $a = $errors[$i];
            return $a;
        }else { return " ";}
    }
    
}

function e_error($errors){
    $h = count($errors);
    for ($i=0; $i < $h; $i++) { 
        if (empty($errors)) {return "  ";}elseif($errors[$i]==="The email Format is not right"||$errors[$i]==="Email is required"){
            return $errors[$i];
        }else { return " ";}
    }
    
}


require "views/contact.views.php";
