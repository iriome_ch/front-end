<nav class="navbar navbar-fixed-top navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand page-scroll" href="#page-top">
        <span>[PHOTO]</span>
      </a>
    </div>
    <div class="collapse navbar-collapse navbar-right" id="menu">
      <ul class="nav navbar-nav">
        <li class="<?= (r_Nav_P('index') ? 'active' : ' '); ?> lien"><a href="<?= (r_Nav_P('index') ? '#' : 'index.php'); ?>"><i class="fa fa-home sr-icons"></i> Home</a></li>
        <li class="<?= (r_Nav_P('about') ? 'active' : ' '); ?> lien"><a href="<?= (r_Nav_P('about') ? '#' : 'about.php'); ?>"><i class="fa fa-bookmark sr-icons"></i> About</a></li>
        <li class="<?= (r_Nav_P('blog') ? 'active' : ' '); ?> lien"><a href="<?= (r_Nav_P('blog') ? '#' : 'blog.php'); ?>"><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
        <li class="<?= (r_Nav_P('contact') ? 'active' : ' '); ?>"><a href="<?= (r_Nav_P('contact') ? '#' : 'contact.php'); ?>"><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>
      </ul>
    </div>
  </div>
</nav>